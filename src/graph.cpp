//
// Created by jerab on 21.11.2022.
//

#include "graph.h"

#include <iostream>
#include <queue>
#include <limits>
#include <sstream>

using namespace std;

Graph::Graph(int numOfVertices) {
    this->numOfVertices = numOfVertices;
    this->adjacency_matrix = vector<vector<edge>>(numOfVertices);
}

bool Graph::addEdge(int from, int to, int weight) {
    if(/*from == to ||*/ from < 0 || to < 0 || weight < 0){
        cerr << "invalid edge input!" << endl;
        return false;
    }
    adjacency_matrix[from].push_back(make_pair(to, weight));
    adjacency_matrix[to].push_back(make_pair(from, weight));
    return true;
}

string Graph::shortestPaths(int source) {
    priority_queue<edge, vector<edge>, greater<>> pq;
    vector<int> distances(numOfVertices, numeric_limits<int>::max());
    vector<int> predecessors(numOfVertices, -1);

    distances[source] = 0;

    pq.push(make_pair(0, source));

    while(!pq.empty()){
        auto u = pq.top().second;
        pq.pop();

        for(auto n : adjacency_matrix[u]){
            auto alt = distances[u] + n.second;
            if(alt < distances[n.first]){
                distances[n.first] = alt;
                predecessors[n.first] = u;
                pq.push(make_pair(alt, n.first));
            }
        }
    }

    std::ostringstream oss;
    oss << "Source: " << source << endl;
    oss << "Vertex \t Distance \t Path" << endl;
    for (int i = 0; i < numOfVertices; ++i) {
        if(i!=source) {
            oss << i << " \t\t " << distances[i] << "\t\t\t";
            printPath(predecessors, i, oss);
        }
    }
    oss << endl;
    return oss.str();
}

void Graph::printPath(vector<int> predecessors, int dest, ostringstream &oss) {
    int p = predecessors[dest];
    string path = "-" + to_string(dest);
    while(p!=-1){
        path.insert(0,to_string(p));
        p = predecessors[p];
        if(p!=-1){
            path.insert(0, "-");
        }
    }
    oss << " " << path << endl;
}

void Graph::printGraph() {
    for(int i=0; i<numOfVertices; ++i){
        cout << i;
        for(auto & j : adjacency_matrix[i]){
            cout << " [" << j.first << "," << j.second << "]";
        }
        cout << endl;
    }
}

int Graph::size() const {
    return this->numOfVertices;
}
