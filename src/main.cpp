//
// Created by jerab on 21.11.2022.
//
#include <iostream>
#include <string>
#include <cstring>
#include <fstream>
#include <chrono>
#include <thread>

#include "graph.h"

using namespace std;

vector<string>* results;
Graph *g;

void worker_thread(int start, int end){
    for(int i=start; i<end; ++i){
        (*results)[i] = g->shortestPaths(i);
    }
}

int main(int argc, char *argv[]){

    if(argc > 1 && strcmp(argv[1], "--help") == 0){
        cout << "Usage: <inputFile> <outputFile> [parallel]" << endl;
        return 0;
    }

    if(argc < 3 || argc > 4){
        cout << "invalid arguments try running with --help." << endl;
        return 0;
    }
    bool parallel = false;
    if(argc == 4) {
        int par = strcmp(argv[3], "parallel");
        if (par == 0) {
            parallel = true;
        } else {
            cout << "invalid arguments try running with --help." << endl;
            return 0;
        }
    }

    //init graph
    ifstream input(argv[1]);
    if(input.is_open()){
        int numOfVertices, numOfEdges;
        input >> numOfVertices;
        input >> numOfEdges;
        g = new Graph(numOfVertices);
        for(int i=0; i<numOfEdges; ++i){
            int s,d,w;
            input >> s;
            input >> d;
            input >> w;
            g->addEdge(s,d,w);
        }
    } else {
        cerr << "could not open input file!" << endl;
        return 1;
    }

    ofstream output;
    output.open(argv[2]);
    if(!output.is_open()){
        cerr << "could not open output file!" << endl;
        return 1;
    }

    auto start = chrono::high_resolution_clock::now();

    if(!parallel) {
        cout << "Serial version for " << g->size() << " vertices ";
        for(int i=0; i<g->size(); ++i){
            output << g->shortestPaths(i);
        }
    } else {
        auto numOfThreads = thread::hardware_concurrency();
        unsigned int size = g->size();
        if(size < numOfThreads){
            cout << "More threads than vertices - using only " << size << " instead of " << numOfThreads << " threads." << endl;
            numOfThreads = size;
        }
        cout << "Parallel version for " << g->size() << " vertices using " << numOfThreads << " threads ";
        unsigned int sourcesPerThread = size/numOfThreads;
        results = new vector<string>(size);
        vector<thread> t(numOfThreads);
        for(unsigned int i=0; i<numOfThreads; ++i){
            if(i < numOfThreads-1) {
                t[i] = thread(worker_thread, i * sourcesPerThread, (i + 1) * sourcesPerThread);
            } else {
                t[i] = thread(worker_thread, i * sourcesPerThread, size);
            }
        }
        //wait for threads
        for(auto & i : t){
            i.join();
        }
        for(int i=0; i<g->size(); ++i){
            output << (*results)[i];
        }
        delete results;
    }
    auto stop = chrono::high_resolution_clock::now();

    auto duration = chrono::duration_cast<chrono::milliseconds>(stop - start);
    cout << "took: " << duration.count() << "ms" << endl;

    delete g;
    output.close();

    return 0;
}