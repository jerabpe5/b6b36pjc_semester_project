//
// Created by jerab on 21.11.2022.
//

#ifndef SHORTESTPATHS_GRAPH_H
#define SHORTESTPATHS_GRAPH_H

#include <utility>
#include <vector>
#include <string>
#include <sstream>

using namespace std;

typedef pair<int,int> edge;

class Graph {
    int numOfVertices;

    vector<vector<edge>> adjacency_matrix;
public:
    explicit Graph(int numOfVertices);

    bool addEdge(int from, int to, int weight);

    string shortestPaths(int source);

    void printGraph();

    static void printPath(vector<int> predecessors, int dest, ostringstream &oss);

    int size() const;

};


#endif //SHORTESTPATHS_GRAPH_H
