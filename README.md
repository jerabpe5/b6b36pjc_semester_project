# B6B36PJC semester project

Shortest paths algorithm implementation in C++.
### Assignment
    Implement the shortest path algorithm that takes a graph as input and calculates shortest
    paths between all vertices and ouptuts the paths and their lengths.
    
### Implementation
    The program takes input file, output file and optional parallel flag
    and calculates shortest paths between all vertices using Dijkstra's algorithm.
    
    In parallel version calculations are split between threads,
    each thread calculates the shortest paths from numberOfVertices/availableThreads sources.

### Input file format:
    First line with 2 integers: number of vertices and number of edges
    then N lines with 3 integers representing edges: node, node, weight (N=number of edges)

### Output file format:
    First line 1 integer: source vertex id
    then V-1 lines of 2 integers: destination vertex and path cost (V=number of vertices)

## Runtime Performance
    V = number of graph vertices
    E = number of edges in graph
    The measured time values are only for the shortest path part of the program
    the rest of the runtime is reading input, creating the graph and writing output to file.

    Values were measured on a laptop with 12th Gen Intel(R) Core(TM) i7-1260P 2.10 GHz (16 threads) and 16 GB RAM.
|          | V=4 E=4 | V=600 E=6000 | V=2000 E=40000 | V=10000 E=1000000 |
|----------|---------|--------------|----------------|-------------------|
| serial   | 61 μs   | 102 ms       | 2,1 s          | 2 min 44 s        |
| parallel | 695 μs  | 48 ms        | 347 ms         | 36,3 s            |

    Below graph shows the values compared.
![figure1](img/figure1.png)

    This graph shows the comparison in logarithmic (base 10) scale.
![figure2](img/figure2.png)
    
    In conclusion with the simple parallelization we achieved speedup of 4 for the biggest instance.
